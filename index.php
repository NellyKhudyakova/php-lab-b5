<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 №B-5</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 №B-5</h1></div>
    <div></div>
</header>

<main>
    <?php

    if (isset($_POST['button'])) {

        $MAX_DEEP = 2;

        function getHTMLcode($url)
        {
            try {
                if (!$ch = curl_init($url)) {
                    throw new Exception();
                }
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                $ret = curl_exec($ch);
                curl_close($ch);
                return $ret;
            } catch (Exception $e) {
                return @file_get_contents($url);
            }
        }

        function getALLtag($text, $tag)
        {
            $exeptTag = '<' . $tag . '>';

            $pattern = '#<' . $tag . '([\s]+[^>]*|)>(.*?)<\/' . $tag . '>#i';

            preg_match_all($pattern, $text, $ret, PREG_SET_ORDER);

            foreach ($ret as $k => $v) {
                if ($tag == 'a') {
                    $href = '';
                    preg_match('#(.*)href="(.*?)"#i', $v[1], $arr);
                    if ($arr) {
                        $href = $arr[2];
                    }
                    $ret[$k] = array('href' => $href, 'text' => ($v[2] = strip_tags($v[2], $exeptTag)));
                } else
                    $ret[$k] = array('text' => ($v[2] = strip_tags($v[2], $exeptTag)));
            }
            return $ret;
        }

        function getINFO($url, $deep)
        {
            global $MAX_DEEP;
            if ($deep > $MAX_DEEP) return;

            if ($deep > 1 && $url == $_POST['url']) return;

            $resultArr = [];
            $code = getHTMLcode($url);

            $resultArr['titles'] = getALLtag($code, 'title');
            $resultArr['descriptions'] = getALLtag($code, 'description');
            $resultArr['keywords'] = getALLtag($code, 'keywords');
            $resultArr['h1'] = getALLtag($code, 'h1');
            $resultArr['h2'] = getALLtag($code, 'h2');

            $resultArr['a'] = getALLtag($code, 'a');

            foreach ($resultArr['a'] as $k => $link) {
                if (empty($resultArr['a'][$k]['href'])) {
                    unset($resultArr['a'][$k]);
                    continue;
                }
                $resultArr['a'][$k]['type'] = getLINKtype($resultArr['a'][$k]['href'], $url);
                $domen = parse_url($url, PHP_URL_HOST);

                if ($resultArr['a'][$k]['type'] == 3) {
                    if ($resultArr['a'][$k]['href'][0] == '/') {
                        $resultArr['a'][$k]['href'] = 'http://' . $domen . $resultArr['a'][$k]['href'];
                    } else {
                        $resultArr['a'][$k]['href'] = 'http://' . $domen . '/' . $resultArr['a'][$k]['href'];
                    }
                }

                if ($resultArr['a'][$k]['type'] !== 1) {
                    $resultArr['a'][$k]['info'] = getINFO($resultArr['a'][$k]['href'], $deep + 1);
                }
            }
            return $resultArr;
        }

        function getLINKtype($href, $url)
        {
            if (strpos($href, '//') === false) return 3; // если в адресе ссылки нет протокола – ссылка локальная

            // выедляем в ссылке имя сервера
            $domen = parse_url($href, PHP_URL_HOST);


            if ($domen == parse_url($url, PHP_URL_HOST)) { // если имя сервера в ссылке равно текущему имени сервера
                return 2; // глобальная ссылка на этот же сайт
            }
            return 1; // иначе ссылку считаем глобальной
        }

        function printInfo($infoArr)
        {
            if (empty($infoArr)) {
                return '';
            }
            $htmlCode = '<div class="infoBlock">';
            foreach ($infoArr as $key => $value) {
                $htmlCode .= '<h3>' . $key . '</h3>';

                if (count($value) < 1) {
                    $htmlCode .= '<p>Не встречается на странице</p>';
                } else if ($key == 'a') {

                    $gLinks = '';
                    $glLinks = '';
                    $lLinks = '';


                    foreach ($value as $K2 => $v2) {
                        $aCode = '<a href="' . $value[$K2]['href'] . '">';

                        if (empty($value[$K2]['text'])) {
                            $aCode .= $value[$K2]['href'];
                        } else {
                            $aCode .= $value[$K2]['text'];
                        }

                        $aCode .= '</a>';

                        switch ($value[$K2]['type']) {
                            case 1:
                                $gLinks .= '<li>' . $aCode . '</li>';
                                break;
                            case 2:
                                $glLinks .= '<li>' . $aCode . '</li>' . printInfo($value[$K2]['info']);
                                break;
                            case 3:
                                $lLinks .= '<li>' . $aCode . '</li>' . printInfo($value[$K2]['info']);

//                                printInfo($value[$K2]['info']);
                                break;
                        }
                    }
                    $htmlCode .= '<h4>Глобальные ссылки</h4><ul>' . $gLinks . '</ul>';
                    $htmlCode .= '<h4>Глобальные ссылки на этот же сайт</h4><ul>' . $glLinks . '</ul>';
                    $htmlCode .= '<h4>Локальные ссылки</h4><ul>' . $lLinks . '</ul>';

                } else {
                    $htmlCode .= '<ul>';
                    for ($i = 0; $i < count($value); $i++) {
                        $htmlCode .= '<li>' . $value[$i]['text'] . '</li>';
                    }
                    $htmlCode .= '</ul>';
                }
            }
            $htmlCode .= '</div>';
            return $htmlCode;
        }

//------------------------- output -------------------------------------------------------

        echo '<div class="back"><a class="back" href="/php-lab-B5/index.php">Обратно</a></div>';

        echo '<h2>Анализируемый URL ' . $_POST['url'] . '</h2>';

        echo '<div id="output">';

        echo printInfo(getINFO($_POST['url'], 1));
//        printInfo(getINFO($_POST['url'], 1));
        echo '</div>';

        echo '<div class="back" style="text-align: center"><a class="back" href="/php-lab-B5/index.php">Обратно</a></div>';

//------------------------- output -------------------------------------------------------
    } else {
        echo '
    <form name="url_form" method="post" action="#">
        <input type="text" name="url" id="url" placeholder="Введите URL">
        <input type="submit" name="button" value="Анализ">
    </form>';
    }
    ?>

</main>


<footer>

</footer>

</body>
</html>